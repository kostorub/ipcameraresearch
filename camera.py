class Camera:
    ip = "0.0.0.0"
    port = 0
    user = ""
    password = ""
    name = ""
    url_path = ""

    def __init__(self, protocol, ip, port, user, password, name, url_path):
        self.protocol = protocol
        self.ip = ip
        self.port = port
        self.user = user
        self.password = password
        self.name = name
        self.url_path = url_path

        self.get_camera_url_methods = {
            "rtsp": self.get_camera_url_rtsp,
            "http": self.get_camera_url_http,
        }

    def get_camera_url(self):
        """
        Получить URL для захвата изображения камеры
        :return: str
        """
        return self.get_camera_url_methods[self.protocol]()

    def get_camera_url_rtsp(self):
        """
        Получить URL для захвата изображения камеры по rtsp
        :return: str
        """
        return "rtsp://{0}:{1}@{2}:{3}{4}".format(self.user, self.password, self.ip, self.port, self.url_path)

    def get_camera_url_http(self):
        """
        Получить URL для захвата изображения камеры по http
        :return: str
        """
        return "http://{2}:{3}".format(self.user, self.password, self.ip, self.port)
