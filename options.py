import json
import logging
import camera


options = {}
options_file_name = "ipcameraresearch.json"
# options_file_name = "ipcameraresearch_work.json"

logger = logging.getLogger("ipcameraresearch")

camera_list = []

try:
    with open(options_file_name, "r", encoding="utf-8") as f:
        data = f.read()
        options = json.loads(data)
except FileNotFoundError:
    logger.error("FileNotFoundError: {0}".format(options_file_name))
except json.JSONDecodeError:
    logger.error("JSONDecodeError: {0}".format(data))

for c in options["camera_list"]:
    if c["status"]:
        camera_list.append(camera.Camera(c["protocol"],
                                         c["ip"],
                                         c["port"],
                                         c["user"],
                                         c["password"],
                                         c["name"],
                                         c["url_path"]))
