import base64
import datetime

import cv2 as cv
import logging
import threading
import requests


class CameraHandler(threading.Thread):
    logger = None
    camera = None
    close_message = "undefined"

    def __init__(self, camera):
        threading.Thread.__init__(self)
        self.logger = logging.getLogger("ipcameraresearch")
        self.camera = camera

    def run(self):
        """
        Начать захват видео
        :return: None
        """

        self.logger.info("Start capture camera {0}".format(self.camera.name))
        # Захват видео потока
        cap = cv.VideoCapture(self.camera.get_camera_url())

        if not cap.isOpened():
            self.close_message = "no connection"

        while cap.isOpened():
            ret, frame = cap.read()
            if ret:
                frame_json = {
                    "date": datetime.datetime.now().timestamp(),
                    "camera_id": 0,
                    "image": str(base64.b64encode(cv.imencode(".jpeg", frame)[1]))
                }

                requests.post("http://127.0.0.1:5000/frame", json=frame_json)
            else:
                self.close_message = "connection closed"

        self.logger.info("Camera {0} close: {1}".format(self.camera.name, self.close_message))

        cap.release()
