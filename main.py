import os
import logging
import logger_handler
import options
import camera_handler


FOLDER_FRAMES = "frames"
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

logger = None


def main():
    global logger

    # Инициализация логирования
    logger_handler.init_logger(logging.DEBUG)
    logger = logging.getLogger("ipcameraresearch")

    # Путь к папке для кадров
    folder_frames_path = os.path.join(ROOT_DIR, FOLDER_FRAMES)
    if not os.path.exists(folder_frames_path):
        os.mkdir(folder_frames_path)

    camera_handler_list = []

    for cam in options.camera_list:
        # Инициализация обработчика камеры
        cam_handler = camera_handler.CameraHandler(cam)
        cam_handler.start()

        # Добавляем в список для хранения
        camera_handler_list.append(cam_handler)


if __name__ == "__main__":
    main()
