import logging


def init_logger(level=logging.INFO):
    """
    Инициализация логирования
    :param level: Уровень логирования
    :return: None
    """

    logger = logging.getLogger("ipcameraresearch")
    logger.setLevel(level)

    ch = logging.FileHandler(filename="ipcameraresearch.log", encoding="utf-8")
    ch.setLevel(level)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    ch.setFormatter(formatter)

    logger.addHandler(ch)